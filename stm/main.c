/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stdio.h"
#include "stdlib.h"
#define DATA_SIZE 1000
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
//declaring states
float systemp;
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef enum
{
// write (our states)enums here
Initial_Temp,
Capture_State,
Checking_State,
TurnOff_State,
FanOn_State,
}
eSystemState;
/* Prototype Event Handlers */
eSystemState InitialTemp(void) // define systemstate
{// TSENSOR AND FAN ARE PIN NAMES
  HAL_GPIO_WritePin(GPIOB, TSENSOR_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(GPIOB, FAN_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(GPIOB, RPI_Pin, GPIO_PIN_RESET);

  HAL_Delay(1*1000); //10 seconds
  return Capture_State;
}

eSystemState FanOnState(void)
{
  HAL_GPIO_WritePin(GPIOB, TSENSOR_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(GPIOB, FAN_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(GPIOB, RPI_Pin, GPIO_PIN_RESET);
  HAL_Delay(1*1000); //10 seconds
  return Capture_State;
}

eSystemState CheckingState(void)
{
  //If temp goes above then on else off
  HAL_GPIO_WritePin(GPIOB, TSENSOR_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(GPIOB, FAN_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(GPIOB, RPI_Pin, GPIO_PIN_RESET);
  HAL_Delay(1*1000); //10 seconds

  //if temp (measured in Farenheit) is above specified temp then TurnOn_State is engaged
  if (systemp > 80)
    return FanOn_State;
  else if (systemp < 50)
    return TurnOff_State;
  else
    return Capture_State;
}

eSystemState TurnOffState(void)
{

  HAL_GPIO_WritePin(GPIOB, TSENSOR_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(GPIOB, FAN_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(GPIOB, RPI_Pin, GPIO_PIN_RESET);
  HAL_Delay(1*1000); //10 seconds
  return Capture_State;
}

eSystemState CaptureState(void)
{
  //Get the temperature
char data [DATA_SIZE];

float  millideg, deg;
FILE *sensor;
int n;

  sensor = fopen("data/extra.txt","w");
  n = scanf(sensor,"%f",&millideg);

  if(sensor == NULL)
  {
    printf("Error!");
    exit(1);
  }
  fclose(sensor);
  deg = millideg/ 1000;

  //Convert systemp from Celcius to Fahrenheit
   systemp= (deg* 1.8)+ 32;

}
// comment to send data needed..ih

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART2_UART_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  /* Attempt at intergrating python

  FILE *fp = fopen("write.txt","w");
  fputs("Real Python!",fp);
  fclose(fp);
  */
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */
  eSystemState eNextState = Initial_Temp;
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
    switch(eNextState){
      case Capture_State:
        eNextState = CaptureState();
        break;
      case Checking_State:
        eNextState = CheckingState();
        break;
      case TurnOff_State:
        eNextState = TurnOffState();
        break;
      case FanOn_State:
        eNextState = FanOnState();
        break;
      default:
        eNextState = InitialTemp();
        break;
    }
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 38400;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pins : TSENSOR_Pin RPI_Pin FAN_Pin */
  GPIO_InitStruct.Pin = TSENSOR_Pin|RPI_Pin|FAN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/