#print('hi')
import time

from gpiozero import OutputDevice

MAX_TEMP = 80  # (degress in Farenheiet) The fan turns on at this temp
MIN_TEMP = 50  # (degress in Farenheiet ) The fan will shut off at this temp
SLEEP_INTERVAL = 10  # (number of seconds) How often the  temperature is being checked
GPIO_PIN = 27  #the GPIO pin being used to control the fan

def get_temp():

""" Get the core temperature.
    Read file from /sys to get laptop temperature in temp in F *1000
    Returns:
        int: The temperature in thousanths of degrees Farenheit """
    
    with open('/sys/class/thermal/thermal_zone0/temp') as f:
        temp_str = f.read()

    try:
        return int(temp_str) / 1000
    except (IndexError, ValueError,) as e:
        raise RuntimeError('Could not analyze ouput temperarure.') from e

#Validate MAX_TEMP and MIN_TEMP
if __name__ == '__main__':
    if MAX_TEMP=<MIN_TEMP:
      raise RuntimeError ('MAX_TEMP needs to be more than MIN_TEMP')
  
    fan=OutputDevice(GPIO_PIN)
 
 while True:
     fanSM = Fan()
