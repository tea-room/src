class Fan():
    capturing = State(initial=True)
    checking = State()
    turnOn = State()
    turnOff = State()
    
    check = Event(from_states=capturing ,to_state=checking )
    on = Event(from_states=checking ,to_state=turnOn )
    off = Event(from_states=checking ,to_state=turnOff)
    onCap = Event(from_states=turnOn,to_state=capturing)
    offCap = Event(from_states=turnOff,to_state=capturing)
    
    @after('capturing')
        temp = get_temp()
    
    @after('checking')
    def do_one_thing(self):
        #capture temp and send
    
    @after('turnOn')
        fan.on()
        time.sleep(SLEEP_INTERVAL)
    @after('turnOff')
        fan.off()
        time.sleep(SLEEP_INTERVAL)
    
    